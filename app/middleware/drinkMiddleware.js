const getAllDrink = (req,res,next)=>{
    console.log("Get All Drinks!");
    next();
};

const getADrink = (req,res,next)=>{
    console.log("Get A Drink!");
    next();
};

const postADrink = (req,res,next)=>{
    console.log("Create New Drink!");
    next();
};

const putADrink = (req,res,next)=>{
    console.log("Update A Drink!");
    next();
};

const deleteADrink = (req,res,next)=>{
    console.log("Delete A Drink!");
    next();
};

module.exports = {
    getAllDrink,
    getADrink,
    postADrink,
    putADrink,
    deleteADrink
}