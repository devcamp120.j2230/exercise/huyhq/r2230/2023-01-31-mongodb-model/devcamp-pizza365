const getAllVoucher = (req,res,next)=>{
    console.log("Get All Vouchers!");
    next();
};

const getAVoucher = (req,res,next)=>{
    console.log("Get A Voucher!");
    next();
};

const postAVoucher = (req,res,next)=>{
    console.log("Create New Voucher!");
    next();
};

const putAVoucher = (req,res,next)=>{
    console.log("Update A Voucher!");
    next();
};

const deleteAVoucher = (req,res,next)=>{
    console.log("Delete A Voucher!");
    next();
};

module.exports = {
    getAllVoucher,
    getAVoucher,
    postAVoucher,
    putAVoucher,
    deleteAVoucher
}